var knex = require('../data/config.js');
var bookshelf = require('bookshelf')(knex);

var Email = module.exports.Email = bookshelf.Model.extend({
  tableName: 'beta_email'
});

var Emails = module.exports.Emails = bookshelf.Collection.extend({
  model: Email
});

