var parse = require('co-body');
var Email = require('./models/email.js').Email;
var bookshelf = require('bookshelf');

module.exports.showHome = function*() {
  yield this.render('index');
};

module.exports.about = function*() {
  yield this.render('about');
};

module.exports.registered = function*() {
  yield this.render('registered');
};

module.exports.register = function*() {
  var emailFromRequest =
    yield parse.form(this);

  emailFromRequest.created_at = new Date();

  yield Email.forge(emailFromRequest).save();

  this.redirect('/registered');
};
