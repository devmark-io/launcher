var knex = require('./config.js');

knex.schema.createTable('beta_email', function(table) {
  table.bigIncrements();
  table.string('address');
  table.timestamp('created_at');
}).then(function() {
  console.log('beta_email table created.');
  process.exit(0);
}).otherwise(function(error) {
  throw error;
});

