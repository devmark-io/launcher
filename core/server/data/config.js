var knex = require('knex')({
  client: 'pg',
  connection: {
    host: process.env.DATABASE_URL,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    charset: 'utf8'
  }
});

module.exports = knex;
