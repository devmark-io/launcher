// get environmental variables
require("dotenv").load();

// 3rd party objects
var koa = require("koa");
var serve = require("koa-static");
var koaroute = require("koa-route");
var jade = require('koa-jade');
var app = module.exports = koa();

// in app objects
var routes = require("./routes.js");

// in app data objects
var assets = require('./assets.json');
var config = require('./config.json');
var settings = require('./settings.json');

var url = process.env.SCHEME + '://' + process.env.DOMAIN + ":" + process.env.PORT;

// jade middleware for koa
app.use(jade.middleware({
  viewPath: __dirname + '/views',
  debug: process.env.DEBUG,
  locals: {
    "assets": assets
  }
}));

// general routing
app.use(koaroute.get("/", routes.showHome));
app.use(koaroute.post("/register", routes.register));
app.use(koaroute.get("/registered", routes.registered));
app.use(koaroute.get("/about", routes.about));

// static routing
app.use(serve(__dirname + "/public"));

var port = process.env.PORT || 3010;
app.listen(port);

console.log(process.env.TARGET + " has started listening on " + url);

process.on('exit', function() {
  console.log(process.env.TARGET + " has stopped listening on " + url);
});
