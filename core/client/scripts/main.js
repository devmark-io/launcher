$(document).ready(function() {
  $('#registerForm').formValidation({
    // validating Bootstrap form
    framework: 'bootstrap',

    // Feedback icons
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },

    // List of fields and their validation rules
    fields: {
      address: {
        validators: {
          notEmpty: {
            message: 'An email is required'
          },
          emailAddress: {
            message: 'Not a valid email address'
          }
        }
      }
    }
  });
});

