var dotenv = require('dotenv');
dotenv._getKeysAndValuesFromEnvFilePath('test/.env');
dotenv._setEnvs();

var co = require("co");
var app = require("../core/server/app.js");
var request = require("co-supertest").agent(process.env.SCHEME +
                                            "://" +
                                            process.env.DOMAIN +
                                            ":" +
                                            process.env.PORT);

describe("devmark http api", function() {
  it("can access home page", function*(done) {
    var url = "/";

    // get via api
    yield request
      .get(url)
      .set("Accept", "text/html")
      .expect("Content-type", "text/html; charset=utf-8")
      .expect(200,done);
  });
});
