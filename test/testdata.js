module.exports = function() {
  return [{
    name: 'google',
    title: 'Google',
    url: 'http://google.com',
    description: 'search engine'
  }, {
    name: 'My Blog',
    title: 'peadardoyle.com',
    url: 'http://peadardoyle.com',
    description: 'a super fantastic blogging experience!'
  }, {
    name: 'Feedreader',
    title: 'Feedreader Online',
    url: 'http://feedreader.com/online/#/reader/category/all/',
    description: 'an online rss reader'
  }, {
    name: 'BitBucket',
    title: 'peadardoyle / home – BitBucket',
    url: 'https://bitbucket.org/',
    description: 'an online repository for git and hg'
  }, {
    name: 'GitHub',
    title: 'GitHub',
    url: 'https://github.com',
    description: 'an online repository for git'
  }, {
    name: 'SO',
    title: 'Stack Overflow',
    url: 'stackoverflow.com',
    description: 'an online Q&A site for programmers'
  }, {
    name: 'My Codepens',
    title: 'Peadar Doyle on Codepen',
    url: 'http://codepen.io/peadar/',
    description: 'a playpen for js, css and html code'
  }, {
    name: 'MDN',
    title: 'Mozilla Developer Network',
    url: 'https://developer.mozilla.org/en-US/',
    description: 'the Mozilla web development resource hub'
  }, {
    name: 'Gandi',
    title: 'Domain name registar and VPS cloud hosting – Gandi.net',
    url: 'https://www.gandi.net/',
    description: 'domain registrar and cloud hosting provider'
  }, {
    name: 'Dictionary',
    title: 'Webster\'s Revised and Unabridged Dictionary (1913) – The ARTFL Project',
    url: 'http://machaut.uchicago.edu/websters',
    description: 'an old dictionary with some pretty great word descriptions and word usage examples'
  }, {
    name: 'Herofuckingu',
    title: 'Personal Apps | Heroku',
    url: 'https://dashboard.heroku.com/apps',
    description: 'PaaS'
  }].map(function(mark) {
    mark.updated_at = mark.created_at = new Date();
    return mark;
  });
};
